package com.oc.service;

public interface PythonService {

	public String compilePython(String source);
}
