package com.oc.serviceimpl;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import org.springframework.stereotype.Service;

import com.oc.service.JavaCompilerService;

@Service
public class JavaCompilerServcieImpl implements JavaCompilerService {

	@Override
	public String javaCompiler(String source) {
		File theDir = null;
		try {
			theDir = new File("D:/programming/java");
			if(!theDir.exists()) {
				theDir.mkdir();
			}
			System.out.println(theDir.getAbsolutePath());
			String programName = "Main.java";
			BufferedWriter out = new BufferedWriter(new FileWriter(new File(theDir.getAbsolutePath() + "/"+ programName),false));
			out.write(source);
			out.close();
			ProcessBuilder build = new ProcessBuilder("javac",theDir.getAbsolutePath() + "/"+ programName);
			build.redirectErrorStream(true);
			build.redirectOutput(new File(theDir.getAbsolutePath() + "/"+"error.log"));
			
			Process process = build.start();
			Thread.sleep(2000);
			
			BufferedReader reader = new BufferedReader(new FileReader(new File(theDir.getAbsolutePath() + "/"+"error.log")));
			StringBuilder logDatabuilder = new StringBuilder();
			String line = null;
			String ls = System.getProperty("line.separator");
			while((line = reader.readLine()) != null) {
				logDatabuilder.append(line);
				logDatabuilder.append(ls);
			}
			reader.close();
			
			String logData = logDatabuilder.toString();
			if(logData.toLowerCase().contains("exception") || logData.toLowerCase().contains("error"))
			{
				return logData;
			}
			
			build = new ProcessBuilder("java ","-cp",theDir.getAbsolutePath()+"/;","Main");
			build.redirectErrorStream(true);
			build.redirectOutput(new File(theDir.getAbsolutePath() + "/"+"output.log"));
			
			process = build.start();
			Thread.sleep(2000);
			
			reader = new BufferedReader(new FileReader(new File(theDir.getAbsolutePath() + "/"+"output.log")));
			logDatabuilder = new StringBuilder();
			line = null;
			while((line = reader.readLine()) != null) {
				logDatabuilder.append(line);
				logDatabuilder.append(ls);
			}
			reader.close();
			
			logData = logDatabuilder.toString();
			
			return logData;
			
		}catch(Exception e) {
			e.printStackTrace();
			try {
			BufferedWriter out = new BufferedWriter(new FileWriter(new File(theDir.getAbsolutePath() + "/"+ "Exception.log"),false));
			out.write(source);
			out.close();}
			catch(Exception ex) {
				e.printStackTrace();
			}
		}finally {
			
		}
		return "Fail";
	}

}
