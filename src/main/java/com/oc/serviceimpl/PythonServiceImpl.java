package com.oc.serviceimpl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import org.springframework.stereotype.Service;

import com.oc.service.PythonService;

@Service
public class PythonServiceImpl implements PythonService {

	@Override
	public String compilePython(String source) {
		File theDir;
		try {
			theDir = new File("programming/python");
			if(!theDir.exists()) {
				theDir.mkdir();
			}
			System.out.println(theDir.getAbsolutePath());
			String programName = "source.py";
			BufferedWriter out = new BufferedWriter(new FileWriter(new File(theDir.getAbsolutePath() + "/"+ programName),false));
			out.write(source);
			out.close();
			ProcessBuilder build = new ProcessBuilder("py",theDir.getAbsolutePath() + "/"+ programName);
			build.redirectErrorStream(true);
			build.redirectOutput(new File(theDir.getAbsolutePath() + "/"+"output.log"));
			
			Process process = build.start();
			Thread.sleep(2000);
			
			BufferedReader reader = new BufferedReader(new FileReader(new File(theDir.getAbsolutePath() + "/"+"output.log")));
			StringBuilder logDatabuilder = new StringBuilder();
			String line = null;
			String ls = System.getProperty("line.separator");
			while((line = reader.readLine()) != null) {
				logDatabuilder.append(line);
				logDatabuilder.append(ls);
			}
			reader.close();
			return logDatabuilder.toString();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			
		}
		return "Fail";
	}

}
