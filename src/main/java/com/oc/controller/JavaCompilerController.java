package com.oc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oc.config.BaseDto;
import com.oc.service.JavaCompilerService;

@RestController
@CrossOrigin
public class JavaCompilerController {

	@Autowired
	JavaCompilerService javaCompilerService;
	@PostMapping("/compilejava")
	public ResponseEntity<BaseDto<String>> javaCompiler(@RequestParam String source) {
		String res = javaCompilerService.javaCompiler(source);
		return new BaseDto<String>(res,"success",HttpStatus.OK).respond();
	}
	
}
