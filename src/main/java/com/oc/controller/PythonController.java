package com.oc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oc.config.BaseDto;
import com.oc.service.PythonService;

@RestController
@CrossOrigin
public class PythonController {

	@Autowired
	private PythonService pythonService; 
	
	@PostMapping("/compilepython")
	public ResponseEntity<BaseDto<String>> pythonCompiler(@RequestParam String source){
		String res = pythonService.compilePython(source);
		return new BaseDto<String>(res, "Success", HttpStatus.OK).respond();
	}
}
