package com.oc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oc.config.BaseDto;
import com.oc.service.CPPService;

@RestController
@CrossOrigin
public class CPPController {

	@Autowired
	private CPPService CPPService;
	
	@PostMapping("/cppcompiler")
	public ResponseEntity<BaseDto<String>> CPPCpmpiler(@RequestParam String source){
		String res = CPPService.CPPCompiler(source);
		return new BaseDto<String>(res, "Success", HttpStatus.OK).respond();
	}
}
